# SEO Blog

This project contains notes and code from studying [Practical GraphQL - Become a GraphQL Ninja](https://www.packtpub.com/application-development/practical-graphql-become-graphql-ninja-video).

## 🏠 [Homepage](https://bitbucket.org/ahristov/practical-graphql)

## Run

Run backend:

```bash
cd backend
yarn
yarn run dev
```

To run query on _playground_, navigate to "http://localhost:8000/graphql", and run:


```javascript

{
  me {
    name
  }
}

```
