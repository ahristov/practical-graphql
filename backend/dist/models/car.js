"use strict";
// Car model
Object.defineProperty(exports, "__esModule", { value: true });
const cars = [{
        id: 1,
        make: "Audi",
        model: "A3",
        color: "dark silver",
        ownedBy: 1
    }, {
        id: 2,
        make: "Mitsubishi",
        model: "Lancer",
        color: "silver",
        ownedBy: 3
    }, {
        id: 3,
        make: "Mini",
        model: "Cooper S",
        color: "black",
        ownedBy: 2
    }, {
        id: 4,
        make: "Porsche",
        model: "Carrera 911",
        color: "yellow",
        ownedBy: 3
    }];
// Cars methods
const carsGet = () => [...cars];
exports.carsGet = carsGet;
const carGet = (id) => cars.filter(u => u.id === id)[0];
exports.carGet = carGet;
const carAdd = (id, make, model, color, ownedBy) => {
    const car = {
        id,
        make,
        model,
        color,
        ownedBy
    };
    cars.push(car);
    return car;
};
exports.carAdd = carAdd;
const carDelete = (id) => {
    const idx = cars.findIndex(c => c.id === id);
    if (idx < 0) {
        return false;
    }
    const car = cars[idx];
    const carId = car.id;
    // remove car from cars
    cars.splice(idx, 1);
    return true;
};
exports.carDelete = carDelete;
const carUnsetOwner = (userId) => {
    cars.forEach((c, i) => {
        if (c.ownedBy === userId) {
            c.ownedBy = null;
        }
    });
};
exports.carUnsetOwner = carUnsetOwner;
//# sourceMappingURL=car.js.map