"use strict";
// User model
Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable-next-line: prefer-const
const users = [{
        id: 1,
        name: "Dani",
        ownedCars: [1]
    }, {
        id: 2,
        name: "Alex",
        ownedCars: [3]
    }, {
        id: 3,
        name: "Atanas",
        ownedCars: [2, 4]
    }];
// User methods
const usersGet = () => [...users];
exports.usersGet = usersGet;
const userGet = (id) => users.filter(u => u.id === id)[0];
exports.userGet = userGet;
const userAdd = (id, name) => {
    const user = {
        id,
        name,
        ownedCars: []
    };
    users.push(user);
    return user;
};
exports.userAdd = userAdd;
const userDelete = (id) => {
    const idx = users.findIndex(u => u.id === id);
    if (idx < 0) {
        return false;
    }
    const user = users[idx];
    const userId = user.id;
    // delete user from users
    users.splice(idx, 1);
    return true;
};
exports.userDelete = userDelete;
const userUnsetOwnedCar = (carId) => {
    users.forEach((u, i) => {
        if (u.ownedCars) {
            const carIdx = u.ownedCars.indexOf(carId);
            if (carIdx > -1) {
                u.ownedCars.splice(carIdx, 1);
            }
        }
    });
};
exports.userUnsetOwnedCar = userUnsetOwnedCar;
//# sourceMappingURL=user.js.map