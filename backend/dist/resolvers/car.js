"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../models");
exports.default = {
    Query: {
        cars: () => models_1.carsGet(),
        car: (parent, { id }) => models_1.carGet(id)
    },
    Mutation: {
        carAdd: (parent, { id, make, model, color }) => models_1.carAdd(id, make, model, color),
        carDelete: (parent, { id }) => {
            models_1.userUnsetOwnedCar(id);
            return models_1.carDelete(id);
        }
    },
    Car: {
        owner: (parent) => {
            // console.log("parent", parent) // parent { id: 3, make: 'Mini', model: 'Cooper S', color: 'black', ownedBy: 2 }
            const user = models_1.usersGet().filter(u => u.id === parent.ownedBy);
            return user[0];
        }
    }
};
//# sourceMappingURL=car.js.map