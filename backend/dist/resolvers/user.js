"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../models");
const me = models_1.usersGet()[0];
exports.default = {
    Query: {
        users: (parent, args, { models }) => models_1.usersGet(),
        user: (parent, { id }) => models_1.userGet(id),
        me: () => me
    },
    Mutation: {
        userAdd: (parent, { id, name }) => models_1.userAdd(id, name),
        userDelete: (parent, { id }) => {
            models_1.carUnsetOwner(id);
            return models_1.userDelete(id);
        },
    },
    User: {
        cars: (parent) => {
            const ownedCars = models_1.carsGet().filter(c => parent.ownedCars.indexOf(c.id) >= 0);
            return ownedCars;
        }
    },
};
//# sourceMappingURL=user.js.map