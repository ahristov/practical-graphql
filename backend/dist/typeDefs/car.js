"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
exports.default = apollo_server_express_1.gql `
type Car {
  id: ID!
  make: String!
  model: String!
  color: String!
  owner: User
}

extend type Query {
  cars: [Car]
  car(id: Int!): Car
}

extend type Mutation {
  carAdd(id: Int!, name: String!): Car!
  carDelete(id: Int!): Boolean
}
`;
//# sourceMappingURL=car.js.map