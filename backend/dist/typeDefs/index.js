"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const default_1 = __importDefault(require("./default"));
const user_1 = __importDefault(require("./user"));
const car_1 = __importDefault(require("./car"));
exports.default = [
    default_1.default,
    user_1.default,
    car_1.default
];
//# sourceMappingURL=index.js.map