"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
exports.default = apollo_server_express_1.gql `
type User {
  id: ID!
  name: String!
  cars: [Car]
}

extend type Query {
  users: [User]
  user(id: Int!): User
  me: User
}

extend type Mutation {
  userAdd(id: Int!, name: String!): User!
  userDelete(id: Int!): Boolean
}
`;
//# sourceMappingURL=user.js.map