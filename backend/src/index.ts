import cors from "cors"
import dotenv from "dotenv"
import express from "express"
import morgan from "morgan"
import { ApolloServer } from 'apollo-server-express'

import * as models from './models'
import typeDefs from './typeDefs'
import resolvers from './resolvers'

// load env variables
dotenv.config()

// app
const app = express()

// middleware
app.use(morgan("dev"))
app.use(cors())

// graphql

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: {
    models
  }
})

server.applyMiddleware({ app })


const port = process.env.PORT || 8000
app.listen(port, () => {
  console.log(`Apollo GraphQL server is running on port ${port}`)
})