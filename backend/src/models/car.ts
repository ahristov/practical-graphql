// Car model

interface ICar {
  id: number,
  make: string,
  model: string,
  color: string,
  ownedBy?: number,
}

const cars:ICar[] = [{
  id: 1,
  make: "Audi",
  model: "A3",
  color: "dark silver",
  ownedBy: 1
}, {
  id: 2,
  make: "Mitsubishi",
  model: "Lancer",
  color: "silver",
  ownedBy: 3
}, {
  id: 3,
  make: "Mini",
  model: "Cooper S",
  color: "black",
  ownedBy: 2
}, {
  id: 4,
  make: "Porsche",
  model: "Carrera 911",
  color: "yellow",
  ownedBy: 3
}]


// Cars methods

const carsGet = (): ICar[] => [...cars]

const carGet = (id: number): ICar => cars.filter(u => u.id === id)[0]

const carAdd = (id: number, make: string, model: string, color: string, ownedBy?: number): ICar => {
  const car: ICar = {
    id,
    make,
    model,
    color,
    ownedBy
  }
  cars.push(car)
  return car
}

const carDelete = (id: number ): boolean => {
  const idx = cars.findIndex(c => c.id === id)
  if (idx < 0) {
    return false
  }

  const car = cars[idx]
  const carId = car.id

  // remove car from cars
  cars.splice(idx, 1)

  return true
}

const carUnsetOwner = (userId: number) => {
  cars.forEach((c, i) => {
    if (c.ownedBy === userId) {
      c.ownedBy = null
    }
  })
}

export {
  ICar,
  carsGet,
  carGet,
  carAdd,
  carDelete,
  carUnsetOwner
}