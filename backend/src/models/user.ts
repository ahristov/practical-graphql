// User model

interface IUser {
  id: number,
  name: string,
  ownedCars?: number[]
}

// tslint:disable-next-line: prefer-const
const users:IUser[] = [{
  id: 1,
  name: "Dani",
  ownedCars: [1]
}, {
  id: 2,
  name: "Alex",
  ownedCars: [3]
}, {
  id: 3,
  name: "Atanas",
  ownedCars: [2, 4]
}]

// User methods

const usersGet = (): IUser[] => [...users]

const userGet = (id: number): IUser => users.filter(u => u.id === id)[0]

const userAdd = (id: number, name: string): IUser => {
  const user: IUser = {
    id,
    name,
    ownedCars: []
  }
  users.push(user)
  return user
}

const userDelete = (id: number): boolean => {
  const idx = users.findIndex(u => u.id === id)
  if (idx < 0) {
    return false
  }

  const user = users[idx]
  const userId = user.id

  // delete user from users
  users.splice(idx, 1)

  return true
}

const userUnsetOwnedCar = (carId: number) => {
  users.forEach((u, i) => {
    if (u.ownedCars) {
      const carIdx = u.ownedCars.indexOf(carId)
      if (carIdx > -1) {
        u.ownedCars.splice(carIdx, 1)
      }
    }
  })
}
export {
  IUser,
  usersGet,
  userGet,
  userAdd,
  userDelete,
  userUnsetOwnedCar
}