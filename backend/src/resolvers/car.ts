import {
  IUser, usersGet, userUnsetOwnedCar,
  ICar, carsGet, carGet, carAdd, carDelete
} from '../models'

export default {
  Query: {
    cars: () => carsGet(),
    car: (parent: any, { id }: { id: number }) => carGet(id)
  },
  Mutation: {
    carAdd: (parent: any, { id, make, model, color}: { id: number, make: string, model:string, color: string}): ICar => carAdd(id, make, model, color),
    carDelete: (parent: any, { id }: { id: number }): boolean => {
      userUnsetOwnedCar(id)
      return carDelete(id)
    }
  },
  Car: {
    owner: (parent: ICar): IUser => {
      // console.log("parent", parent) // parent { id: 3, make: 'Mini', model: 'Cooper S', color: 'black', ownedBy: 2 }
      const user = usersGet().filter(u => u.id === parent.ownedBy)
      return user[0]
    }
  }
}
