import {
  IUser, usersGet, userGet, userAdd, userDelete,
  ICar, carsGet, carUnsetOwner
} from '../models'

const me = usersGet()[0]

export default {
  Query: {
    users: (parent:any, args:any, { models }: { models:any }) => usersGet(),
    user: (parent: any, { id }: { id: number }) => userGet(id),
    me: () => me
  },
  Mutation: {
    userAdd: (parent: any, { id, name}: { id: number, name: string}): IUser => userAdd(id, name),
    userDelete: (parent: any, { id }: { id: number }): boolean => {
      carUnsetOwner(id)
      return userDelete(id)
    },
  },
  User: {
    cars: (parent: IUser): ICar[] => {
      const ownedCars = carsGet().filter(c => parent.ownedCars.indexOf(c.id) >= 0)
      return ownedCars
    }
  },
}
