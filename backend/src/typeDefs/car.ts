import { gql } from 'apollo-server-express'

export default gql`
type Car {
  id: ID!
  make: String!
  model: String!
  color: String!
  owner: User
}

extend type Query {
  cars: [Car]
  car(id: Int!): Car
}

extend type Mutation {
  carAdd(id: Int!, name: String!): Car!
  carDelete(id: Int!): Boolean
}
`
