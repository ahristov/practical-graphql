import defaultTypeDefs from './default'
import userTypeDefs from './user'
import carTypeDefs from './car'

export default [
  defaultTypeDefs,
  userTypeDefs,
  carTypeDefs
]