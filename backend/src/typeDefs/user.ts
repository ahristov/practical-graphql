import { gql } from 'apollo-server-express'

export default gql`
type User {
  id: ID!
  name: String!
  cars: [Car]
}

extend type Query {
  users: [User]
  user(id: Int!): User
  me: User
}

extend type Mutation {
  userAdd(id: Int!, name: String!): User!
  userDelete(id: Int!): Boolean
}
`
